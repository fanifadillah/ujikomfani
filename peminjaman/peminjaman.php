<?php
session_start();
include '../admin/koneksi.php';
if(!isset($_SESSION['username'])) {
  header('location:../login.php');

}
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Inventaris SMK NEGERI 1 CIOMAS</title>

        <meta name="description" content="AppUI is a Web App Bootstrap Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-3.3.1.min.js"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!--
            Available classes:

            'page-loading'      enables page preloader
        -->
        <div id="page-wrapper" class="page-loading">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
                Available #page-container classes:

                'sidebar-light'                                 for a light main sidebar (You can add it along with any other class)

                'sidebar-visible-lg-mini'                       main sidebar condensed - Mini Navigation (> 991px)
                'sidebar-visible-lg-full'                       main sidebar full - Full Navigation (> 991px)

                'sidebar-alt-visible-lg'                        alternative sidebar visible by default (> 991px) (You can add it along with any other class)

                'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
                'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar

                'fixed-width'                                   for a fixed width layout (can only be used with a static header/main sidebar layout)

                'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links (You can add it along with any other class)
            -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <!-- Alternative Sidebar -->
                <div id="sidebar-alt" tabindex="-1" aria-hidden="true">
                    <!-- Toggle Alternative Sidebar Button (visible only in static layout) -->
                    <a href="javascript:void(0)" id="sidebar-alt-close" onclick="App.sidebar('toggle-sidebar-alt');"><i class="fa fa-times"></i></a>

                    <!-- Wrapper for scrolling functionality -->
                    <div id="sidebar-scroll-alt">
                        <!-- Sidebar Content -->
                        <div class="sidebar-content">
                        </div>
                        <!-- END Sidebar Content -->
                    </div>
                    <!-- END Wrapper for scrolling functionality -->
                </div>
                <!-- END Alternative Sidebar -->

               <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <!-- Alternative Sidebar -->
               
                <!-- Main Sidebar -->
                <div id="sidebar">
                    <!-- Sidebar Brand -->
                    <div id="sidebar-brand" class="themed-background">
                        <a href="index.html" class="sidebar-title">
                            <i class="fa fa-cube"></i> <span class="sidebar-nav-mini-hide"><strong>Inventaris</strong></span>
                        </a>
                    </div>
                    <!-- END Sidebar Brand -->

                    <!-- Wrapper for scrolling functionality -->
                    <div id="sidebar-scroll">
                        <!-- Sidebar Content -->
                        <div class="sidebar-content">
                            <!-- Sidebar Navigation -->
                            <ul class="sidebar-nav">
                                <li>
                                    <a href="index.php" class=" active"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                                </li>
                                <li class="sidebar-separator">
                                    <i class="fa fa-ellipsis-h"></i>
                                </li>
                               
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-cart-plus sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Transaksi</span></a>
                                    <ul>

                                        <li>
                                        <a href="peminjaman.php">Peminjaman</a>
                                        </li>

                                        <li>
                                        <a href="pengembalian.php">Pengembalian</a>
                                        </li>
                                
                                    </ul>
                                </li>
                                        

                            <!-- END Sidebar Navigation -->

                            <!-- Color Themes -->
                            <!-- Preview a theme on a page functionality can be found in js/app.js - colorThemePreview() -->
                            <div class="sidebar-section sidebar-nav-mini-hide">
                                <div class="sidebar-separator push">
                                    <i class="fa fa-ellipsis-h"></i>
                                </div>
                                <ul class="sidebar-themes clearfix">
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-default" data-toggle="tooltip" title="Default" data-theme="default" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-default"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-classy" data-toggle="tooltip" title="Classy" data-theme="css/themes/classy.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-classy"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-social" data-toggle="tooltip" title="Social" data-theme="css/themes/social.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-social"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-flat" data-toggle="tooltip" title="Flat" data-theme="css/themes/flat.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-flat"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-amethyst" data-toggle="tooltip" title="Amethyst" data-theme="css/themes/amethyst.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-amethyst"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-creme" data-toggle="tooltip" title="Creme" data-theme="css/themes/creme.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-creme"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-passion" data-toggle="tooltip" title="Passion" data-theme="css/themes/passion.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="">
                                            <span class="section-side themed-background-dark-passion"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-default" data-toggle="tooltip" title="Default + Light Sidebar" data-theme="default" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-classy" data-toggle="tooltip" title="Classy + Light Sidebar" data-theme="css/themes/classy.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-social" data-toggle="tooltip" title="Social + Light Sidebar" data-theme="css/themes/social.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-flat" data-toggle="tooltip" title="Flat + Light Sidebar" data-theme="css/themes/flat.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-amethyst" data-toggle="tooltip" title="Amethyst + Light Sidebar" data-theme="css/themes/amethyst.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-creme" data-toggle="tooltip" title="Creme + Light Sidebar" data-theme="css/themes/creme.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-passion" data-toggle="tooltip" title="Passion + Light Sidebar" data-theme="css/themes/passion.css" data-theme-navbar="navbar-inverse" data-theme-sidebar="sidebar-light">
                                            <span class="section-side"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-default" data-toggle="tooltip" title="Default + Light Header" data-theme="default" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-default"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-classy" data-toggle="tooltip" title="Classy + Light Header" data-theme="css/themes/classy.css" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-classy"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-social" data-toggle="tooltip" title="Social + Light Header" data-theme="css/themes/social.css" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-social"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-flat" data-toggle="tooltip" title="Flat + Light Header" data-theme="css/themes/flat.css" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-flat"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-amethyst" data-toggle="tooltip" title="Amethyst + Light Header" data-theme="css/themes/amethyst.css" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-amethyst"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-creme" data-toggle="tooltip" title="Creme + Light Header" data-theme="css/themes/creme.css" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-creme"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="themed-background-passion" data-toggle="tooltip" title="Passion + Light Header" data-theme="css/themes/passion.css" data-theme-navbar="navbar-default" data-theme-sidebar="">
                                            <span class="section-header"></span>
                                            <span class="section-side themed-background-dark-passion"></span>
                                            <span class="section-content"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- END Color Themes -->
                        </div>
                        <!-- END Sidebar Content -->
                    </div>
                    <!-- END Wrapper for scrolling functionality -->

                    <!-- Sidebar Extra Info -->
                    <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
                        <div class="push-bit">
                            <span class="pull-right">
                                <a href="javascript:void(0)" class="text-muted"></a>
                            </span>
                        </div>
                    </div>
                    <!-- END Sidebar Extra Info -->
                </div>
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    <!-- Header -->
                    <!-- In the PHP version you can set the following options from inc/config file -->
                    <!--
                        Available header.navbar classes:

                        'navbar-default'            for the default light header
                        'navbar-inverse'            for an alternative dark header

                        'navbar-fixed-top'          for a top fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                            'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                        'navbar-fixed-bottom'       for a bottom fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                            'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                    -->
                    <header class="navbar navbar-inverse navbar-fixed-top">
                        <!-- Left Header Navigation -->
                        <ul class="nav navbar-nav-custom">
                            <!-- Main Sidebar Toggle Button -->
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                                    <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
                                </a>
                            </li>
                            <!-- END Main Sidebar Toggle Button -->

                            <!-- Header Link -->
                            <li class="hidden-xs animation-fadeInQuick">
                                <a href=""><strong>WELCOME</strong></a>
                            </li>
                            <!-- END Header Link -->
                        </ul>
                        <!-- END Left Header Navigation -->

                        <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
                            <!-- Search Form -->
                            <li>
                                <form action="page_ready_search_results.html" method="post" class="navbar-form-custom">
                                </form>
                            </li>
                            <!-- END Search Form -->

                            <!-- Alternative Sidebar Toggle Button -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="gi gi-settings"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-header">
                                        <strong>ADMINISTRATOR</strong>
                                    </li>

                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off fa-fw pull-right"></i>
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END Alternative Sidebar Toggle Button -->

                            
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    <!-- END Header -->

                        <!-- Page content -->
                    <div id="page-content">
                        <!-- Table Styles Header -->
                        <div class="content-header">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="header-section">
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs">
                                    <div class="header-section">
                                        <ul class="breadcrumb breadcrumb-top">
                                            <li>User Interface</li>
                                            <li>Elements</li>
                                            <li><a href="">Tables</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content">
    <div class="header">
      <br><br>
        <ul class="breadcrumb">
            <center><h2>Form Peminjaman</h2></center>
        </ul>

    </div>
 

    </div>  <div class="panel-body">
       <div class="row">
        <div class="col-lg-12">
        
                <div class="panel-body">
                    <center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                            <label>Pilih Nama Barang</label>
                            <form method="POST">
                                <select name="id_inventaris" class="form-control m-bot15">
                                    <?php
                                    include "../koneksi.php";
                                //display values in combobox/dropdown
                                    $result = mysqli_query($koneksi,"SELECT id_inventaris,nama from inventaris where jumlah > 0");
                                    while($row = mysqli_fetch_assoc($result))
                                    {
                                        echo "<option value='$row[id_inventaris]'>$row[nama]</option>";
                                    } 
                                    ?>
                                </select>
                                <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-primary">Pilih</button>
                            </form>
                        </div>
                    </div></center>

                    <?php
                    if(isset($_POST['pilih'])){?>
                      <form action="simpan_smtr_detail.php" method="post" enctype="multipart/form-data">
                       <?php
                       include "../koneksi.php";
                       $id_inventaris=$_POST['id_inventaris'];
                       $select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
                       while($data=mysqli_fetch_array($select)){
                        ?>
                        <br>
                        <br>
                    </div>
                </div>
          
                        <div class="row">
                          <div class="col-md-2">ID Inventaris<input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
                              <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly"></div>
                              <div class="col-md-2">Nama<input name="" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly"></div>
                              <div class="col-md-2">Jumlah Barang<input name="jumlah" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly"></div>
                              <div class="col-md-2">Jumlah Pinjam<input name="jumlah_pinjam" required="" type="number" class="form-control" placeholder="Jumlah" autocomplete="off" maxlength="11"></div>
                              <br><button type="submit" class="btn btn-danger">Pinjam</button>
                              <br>
                              <br>

                          <?php  } ?>
                      </form>

                  <?php } ?>
              </div>
              <form action="simpan_pinjam.php" method="post" role="form">
                <?php 
                $status_peminjaman="Pinjam";

                include "../admin/koneksi.php";
                $query ="SELECT max(kode_peminjaman) as maxKode FROM peminjaman";
                $hasil = mysqli_query($query);
                $data = mysqli_fetch_array($hasil);
                $kode_barang = $data['maxKode'];
                $noUrut = (int) substr($kode_barang, 4, 4);
                $noUrut++;
                $char = "PNJ";
                $kode_barang = $char . sprintf("%04s", $noUrut);
                ?>


                <br><br>
                <div class="row">
                  <div class="panel-body">
                      <div class="table-responsive">

                        <div class="col-lg-12">
                          <?php $tanggal_pinjam=gmdate("Y-m-d H:i:s",time()+60*60*7); 
                          $kode_pinjam=$_POST['kode_pinjam'];
                          $status_peminjaman='Pinjam';
                          $status="Y";
                          $auto=mysqli_query($koneksi,"select * from peminjaman order by id_peminjaman desc limit 1");
                          $no=mysqli_fetch_array($auto);
                          $angka=$no['id_peminjaman']+1;  ?>
                          <div class="row" style="margin-left: 550px">
                            <div class="col-md-7 pull-right">Tanggal Pinjam :<input name="tanggal_pinjam" readonly type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $tanggal_pinjam;?>" autocomplete="off" maxlength="11">
                            </div>
                            <div class="col-md-5">Kode Pinjam :<input name="kode_pinjam" readonly type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $kode_barang;?>" autocomplete="off" maxlength="11">
                            </div></div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
       

                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID Detail Pinjam</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Jumlah Pinjam</th>
                                        <th>Nama Pegawai</th>
                                        <th>Option</th>

                                    </tr> 
                                </thead>
                                <tbody>
                                    <?php
                                    include "../admin/koneksi.php";
                                    $select=mysqli_query($koneksi,"select * from smtr_detail left join  
                                        inventaris on smtr_detail.id_inventaris=inventaris.id_inventaris");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                        ?>
                                        <tr>
                                           <td><input name="id_detail_pinjam[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" autocomplete="off" maxlength="11" required="" readonly>
                                            <input name="status[]" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $status;?>" autocomplete="off" maxlength="11" required="" readonly>
                                               <input name="tanggal_kembali" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" autocomplete="off" maxlength="11">
                                               <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $angka;?>" autocomplete="off" maxlength="11">
                                               <input name="status_peminjaman" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $status_peminjaman;?>" autocomplete="off" maxlength="11">
                                               <input name="id_inventaris[]" id="id_inventaris" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11">
                                               <td><input name=""  type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" readonly>
                                                   <td><input name="jumlah[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" readonly >
                                                       <td><input name="jumlah_pinjam[]" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['jumlah_pinjam'];?>" autocomplete="off" maxlength="11" ></td>
                                                       <td><input  name="id_pegawai" autocomplete="off" class="form-control" value="<?php echo $_SESSION['id_pegawai'];?>.<?php echo $_SESSION['nama_pegawai'];?>" rows="3" placeholder="Pilih ID Pegawai" readonly>
                                                        </input>
                                                    <td>
                                                       <a class="btn icon-trash red" href="hapus_smtr_detail.php?id_detail_pinjam=<?php echo $data['id_detail_pinjam']; ?>"></a></td>

                                                       <?php
                                                   }
                                                   ?>

                                               </tr>
                                           </tbody>
                                       </table>
                                       <br>
                                       <button type="submit" class="btn btn-warning">&nbsp;Pinjam</button>
                                       <hr><br>
                                       <br>
                                   </form>
                                   <!-- /.panel-heading -->
                                   <div class="panel-body">
                                    <div class="table-responsive">

                                      
                                            <table class="table">
                                              <thead>
                                                <tr>
                                                  <th>#</th>
                                                  <th>Kode Pinjam</th>
                                                  <th>Tanggal Pinjam</th>
                                                  <th>Nama Pegawai</th>
                                                  <th>Status Peminjaman</th>
                                                  <th>Option</th>
                                                  <th style="width: 3.5em;"></th>
                                              </tr> 
                                          </thead>
                                          <tbody>
                                           <?php
                                           include "../admin/koneksi.php";
                                           $select=mysqli_query($koneksi,"select * from peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai
                                             where status_peminjaman='Pinjam'");
                                           $no=1;
                                           while($data=mysqli_fetch_array($select))
                                           {
                                            ?>
                                            <tr>
                                             <td><?php echo $no++; ?></td>
                                             <td><?php echo $data['kode_pinjam'];?></td>
                                             <td><?php echo $data['tanggal_pinjam']; ?></td>
                                             <td><?php echo $data['nama_pegawai']; ?></td>
                                             <td><span class="label label-success"><?php echo $data['status_peminjaman']; ?></td>
                                                 <td><a class="btn outline btn-primary " href="detail_pinjam.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>"></a></td>

                                             </tr>
                                             <?php
                                         }
                                         ?>



                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
                   </div>
                 </div>
                   </div>
                 </div>
                   </div>
                 </div>
                   </div>
                 </div>
                   </div>
                 </div>

                         
                
                        
<!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>

        <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
       
    </body>
</html>
